#!/bin/bash

usermod -aG ossec apache

cd /var/www/html/ossec-wui/
chmod 770 tmp/
chgrp apache tmp/

cd ..
chown -R apache:apache ossec-wui/
chmod -R 755 ossec-wui/


mkdir /etc/selinux/targeted/ossec-wui
cd /etc/selinux/targeted/ossec-wui

cat > ossec-wui.te <<EOF
module ossec-wui 1.0;

require {
type var_log_t;
    type httpd_t;
    type var_t;
    class file { read getattr open };
}

#============= httpd_t ==============
allow httpd_t var_log_t:file read;
allow httpd_t var_t:file { read getattr open };
EOF

checkmodule -M -m ossec-wui.te -o ossec-wui.mod
semodule_package -o ossec-wui.pp -m ossec-wui.mod
semodule -i ossec-wui.pp

semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/ossec-wui(/.*)?"
restorecon -Rv /var/www/html/ossec-wui/

chown -R ossec:apache /var/ossec/
