#!/bin/bash

mkdir /etc/selinux/targeted/ossec-hids
cd /etc/selinux/targeted/ossec-hids

#package for audit2allow
yum install policycoreutils-python-utils -y


cat > ossec-hids.te <<EOF
module ossec-hids 1.0;

require {
        type init_t;
        type var_t;
        class file { execute read };
}

#============= init_t ==============
allow init_t var_t:file { execute read };
EOF

checkmodule -M -m ossec-hids.te -o ossec-hids.mod
semodule_package -o ossec-hids.pp -m ossec-hids.mod
semodule -i ossec-hids.pp

semanage fcontext -a -t bin_t "/var/ossec/bin/(.*)"
restorecon -rv /var/ossec/bin